package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class RepeatCommandTest {

    @Test
    void execute() {
        final Command mockCommand = Mockito.mock(Command.class);
        final CommandPublisher commandPublisher = new CommandPublisher() {
            @Override
            public void publish(Command command) {
                command.execute();
            }

            @Override
            public void addListener(CommandPublisherListener listener) {
                //do nothing
            }
        };
        final RepeatCommand repeatCommand = new RepeatCommand(mockCommand, commandPublisher, 3);
        repeatCommand.execute();
        Mockito.verify(mockCommand, times(3)).execute();
    }
}