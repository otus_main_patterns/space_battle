package ru.yumashev.otus.main.patterns.ioc;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.yumashev.otus.main.patterns.Command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class IOCImpTest {

    @Test
    void resolve() {
        final IOCImp iocImp = new IOCImp();
        Function<Object[], TestClass> function = args -> new TestClass((String) args[0], (int) args[1]);
        final Command register = iocImp.resolve("IoC.Register", Command.class, "test", function);
        register.execute();
        final TestClass testClass = iocImp.resolve("test", TestClass.class, "test", 123);
        Assertions.assertEquals("test", testClass.a());
        Assertions.assertEquals(123, testClass.b());
    }

    @Test
    void registerMT() {
        final IOCImp iocImp = new IOCImp();
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        AtomicInteger counter = new AtomicInteger(0);
        for (int i = 0; i < 10; i++) {
            executorService
                    .execute(() -> {
                        Function<Object[], TestClass> function = args -> new TestClass((String) args[0], (int) args[1]);
                        final Command register = iocImp.resolve("IoC.Register", Command.class, "test", function);
                        register.execute();
                        counter.incrementAndGet();
                    });
        }
        Awaitility.waitAtMost(5, TimeUnit.SECONDS).until(() -> counter.get() == 10);
        final ArrayList<TestClass> objects = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            executorService
                    .execute(() -> {
                        final TestClass testClass = iocImp.resolve("test", TestClass.class, "test", finalI);
                        Assertions.assertEquals("test", testClass.a());
                        Assertions.assertEquals(finalI, testClass.b());
                        objects.add(testClass);
                    });
        }
        Awaitility.waitAtMost(5, TimeUnit.SECONDS).until(() -> objects.size() == 10);
    }

    public record TestClass(String a, int b) {
    }

    @Test
    void createScopeTest() {
        final IOCImp iocImp = new IOCImp();
        Function<Object[], TestClass> function = args -> new TestClass((String) args[0], (int) args[1]);
        iocImp.resolve("IoC.Register", Command.class, "test", function).execute();
        final TestClass testClassRoot = iocImp.resolve("test", TestClass.class, "testRoot", 0);
        Assertions.assertEquals("testRoot", testClassRoot.a());
        Assertions.assertEquals(0, testClassRoot.b());
        iocImp.resolve("IoC.Scope.Create", Command.class, "1").execute();
        iocImp.resolve("IoC.Scope.Set", Command.class, "1").execute();
        iocImp.resolve("IoC.Register", Command.class, "test", function).execute();
        final TestClass testClass = iocImp.resolve("test", TestClass.class, "test1", 1);
        Assertions.assertEquals("test1", testClass.a());
        Assertions.assertEquals(1, testClass.b());
        iocImp.resolve("IoC.Scope.Create", Command.class, "1.1", "1").execute();
        iocImp.resolve("IoC.Scope.Set", Command.class, "1.1").execute();
        iocImp.resolve("IoC.Register", Command.class, "test", function).execute();
        final TestClass testClass11 = iocImp.resolve("test", TestClass.class, "test11", 11);
        Assertions.assertEquals("test11", testClass11.a());
        Assertions.assertEquals(11, testClass11.b());
        iocImp.resolve("IoC.Scope.Set", Command.class, "root").execute();
        iocImp.resolve("IoC.Scope.Root", Command.class).execute();

    }
}