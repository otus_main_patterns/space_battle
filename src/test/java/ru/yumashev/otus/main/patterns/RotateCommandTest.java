package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

class RotateCommandTest {

    //Для объекта, с углом 0 и поворачивающегося с угловой скоростью 1 движение меняет угол объекта на 1
    @Test
    void okTest() {
        final Rotatable mockRotatable = Mockito.mock(Rotatable.class);
        final int angleBefore = 0;
        final int angularVelocity = 1;
        final int angleAfter = 1;
        Mockito.when(mockRotatable.getAngle()).thenReturn(angleBefore);
        Mockito.when(mockRotatable.getAngularVelocity()).thenReturn(angularVelocity);
        new RotateCommand(mockRotatable).execute();
        Mockito.verify(mockRotatable, Mockito.atMostOnce()).setAngle(angleAfter);
        Mockito.verify(mockRotatable).setAngle(angleAfter);
    }

    //Для объекта, с углом 0 и поворачивающегося с угловой скоростью 180 - ошибка
    @Test
    void tooFastExceptionTest() {
        final Rotatable mockRotatable = Mockito.mock(Rotatable.class);
        final int angleBefore = 0;
        final int angularVelocity = 180;
        Mockito.when(mockRotatable.getAngle()).thenReturn(angleBefore);
        Mockito.when(mockRotatable.getAngularVelocity()).thenReturn(angularVelocity);
        Assertions.assertThrowsExactly(TooFastRotateException.class, () -> new RotateCommand(mockRotatable).execute());
        Mockito.verify(mockRotatable, Mockito.never()).setAngle(anyInt());
    }

}