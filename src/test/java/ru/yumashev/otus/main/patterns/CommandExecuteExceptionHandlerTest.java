package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.times;

class CommandExecuteExceptionHandlerTest {

    @Test
    void registerAndHandleHandlerTest() {
        CommandExecuteExceptionHandler commandExecuteExceptionHandler = new CommandExecuteExceptionHandler();
        final CommandExceptionHandler commandExceptionHandler = Mockito.mock(CommandExceptionHandler.class);
        final CommandExceptionHandler commandExceptionHandler2 = Mockito.mock(CommandExceptionHandler.class);
        final NullPointerException nullPointerException = new NullPointerException();
        final Command command = new TestCommand(nullPointerException);
        final IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        final Command command2 = new TestCommand(illegalArgumentException);
        commandExecuteExceptionHandler.registerHandler(TestCommand.class, NullPointerException.class, commandExceptionHandler);
        commandExecuteExceptionHandler.registerHandler(TestCommand.class, IllegalArgumentException.class, commandExceptionHandler2);
        commandExecuteExceptionHandler.handle(command, nullPointerException);
        commandExecuteExceptionHandler.handle(command2, illegalArgumentException);
        Mockito.verify(commandExceptionHandler, times(1)).handle(command, nullPointerException);
        Mockito.verify(commandExceptionHandler2, times(1)).handle(command2, illegalArgumentException);
    }

    public static class TestCommand implements Command {

        private final RuntimeException exception;

        public TestCommand(RuntimeException exception) {
            this.exception = exception;
        }

        @Override
        public void execute() {
            throw exception;
        }
    }

}