package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.awaitility.Awaitility.await;

class CommandExecutorTest {

    @Test
    void startStopTest() {
        final CommandExecutor commandExecutor = new CommandExecutor(new CommandExecuteExceptionHandler());
        commandExecutor.start();
        Assertions.assertFalse(commandExecutor.isStop());
        commandExecutor.stop();
        Assertions.assertTrue(commandExecutor.isStop());
    }


    @Test
    void executeCommandsTest() {
        final CommandExecutor commandExecutor = new CommandExecutor(new CommandExecuteExceptionHandler());
        commandExecutor.start();
        AtomicInteger inc = new AtomicInteger(1);
        Command command = inc::incrementAndGet;
        commandExecutor.addCommand(command);
        commandExecutor.addCommand(command);
        commandExecutor.addCommand(command);
        commandExecutor.addCommand(command);
        commandExecutor.addCommand(command);
        await().atMost(5, TimeUnit.SECONDS).until(() -> inc.get() != 5);
        commandExecutor.stop();
    }
}