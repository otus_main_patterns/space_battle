package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;

class MoveCommandTest {

    //Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3) движение меняет положение объекта на (5, 8)
    @Test
    void okTest() {
        final Movable mockMovable = Mockito.mock(Movable.class);
        final Coords positionBefore = new Coords(12, 5);
        final Coords velocity = new Coords(-7, 3);
        final Coords positionAfter = new Coords(5, 8);
        Mockito.when(mockMovable.getPosition()).thenReturn(positionBefore);
        Mockito.when(mockMovable.getVelocity()).thenReturn(velocity);
        new MoveCommand(mockMovable).execute();
        Mockito.verify(mockMovable, Mockito.atMostOnce()).setPosition(positionAfter);
        Mockito.verify(mockMovable).setPosition(positionAfter);
    }

    //Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке
    @Test
    void cannotChangePositionExceptionTest() {
        final Movable mockMovable = Mockito.mock(Movable.class);
        final Coords positionBefore = new Coords(12, 5);
        final Coords velocity = new Coords(-7, 3);
        Mockito.when(mockMovable.getPosition()).thenReturn(positionBefore);
        Mockito.when(mockMovable.getVelocity()).thenReturn(velocity);
        Mockito.doThrow(CannotChangePositionException.class).when(mockMovable).setPosition(any());
        Assertions.assertThrowsExactly(CannotChangePositionException.class, () -> {
            new MoveCommand(mockMovable).execute();
        });
    }


}