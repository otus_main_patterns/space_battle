package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.anyInt;

class BurnFuelCommandTest {

    @Test
    void executeWithOkResultTest() {
        final FuelBurnable fuelBurnableMock = Mockito.mock(FuelBurnable.class);
        Mockito.when(fuelBurnableMock.getFuelVelocity()).thenReturn(1);
        Mockito.when(fuelBurnableMock.getFuelLevel()).thenReturn(1);
        final BurnFuelCommand BurnFuelCommand = new BurnFuelCommand(fuelBurnableMock);
        BurnFuelCommand.execute();
        Mockito.verify(fuelBurnableMock, Mockito.times(1)).getFuelVelocity();
        Mockito.verify(fuelBurnableMock, Mockito.times(1)).getFuelLevel();
        Mockito.verify(fuelBurnableMock, Mockito.times(1)).setFuelLevel(anyInt());
        Mockito.verify(fuelBurnableMock).setFuelLevel(0);
    }

    @Test
    void executeWithFailResultTest() {
        final FuelBurnable fuelBurnableMock = Mockito.mock(FuelBurnable.class);
        Mockito.when(fuelBurnableMock.getFuelVelocity()).thenReturn(2);
        Mockito.when(fuelBurnableMock.getFuelLevel()).thenReturn(1);
        final BurnFuelCommand burnFuelCommand = new BurnFuelCommand(fuelBurnableMock);
        Assertions.assertThrowsExactly(CheckFuelCommandException.class, burnFuelCommand::execute);
        Mockito.verify(fuelBurnableMock, Mockito.never()).setFuelLevel(anyInt());
    }

}