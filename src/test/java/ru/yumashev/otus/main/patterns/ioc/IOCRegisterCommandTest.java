package ru.yumashev.otus.main.patterns.ioc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

class IOCRegisterCommandTest {

    @Test
    void execute() {
        final IOC mock = Mockito.mock(IOC.class);
        Map<String, Function<Object[], ?>> map = new HashMap<>();
        Function<Object[], TestClass> function = args -> new TestClass((String) args[0], (int) args[1]);
        final String key = "test";
        final Object[] objects = {key, function};
        new IOCRegisterCommand(mock, objects).execute();
        Mockito.verify(mock, Mockito.atMostOnce()).addResolver(key, function);
    }

    public record TestClass(String a, int b) {
    }


}