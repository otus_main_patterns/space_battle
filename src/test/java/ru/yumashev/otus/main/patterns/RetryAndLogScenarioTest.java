package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.any;

class RetryAndLogScenarioTest {


    @Test
    void executeRetryLog() {
        CommandPublisher commandPublisher = new CommandPublisherImpl();
        CommandExecuteExceptionHandler commandExecuteExceptionHandler = new CommandExecuteExceptionHandler();
        final CommandExecutor commandExecutor = new CommandExecutor(commandExecuteExceptionHandler);
        commandPublisher.addListener(commandExecutor);

        final LogCommandExceptionHandler logCommandExceptionHandler = Mockito.spy(new LogCommandExceptionHandler(commandPublisher));
        final RetryCommandExceptionHandler retryCommandExceptionHandler = Mockito.spy(new RetryCommandExceptionHandler(commandPublisher));


        commandExecuteExceptionHandler.registerHandler(RetryCommand.class, RetryCommandException.class, logCommandExceptionHandler);
        commandExecuteExceptionHandler.registerHandler(RetryCommand.class, Exception.class, retryCommandExceptionHandler);
        commandExecuteExceptionHandler.registerHandler(TestCommand.class, Exception.class, retryCommandExceptionHandler);

        final TestCommand command = new TestCommand(new IllegalArgumentException("Ошибка"));

        final TestCommand spyCommand = Mockito.spy(command);

        commandExecutor.start();

        commandPublisher.publish(spyCommand);


        await().pollDelay(1, TimeUnit.SECONDS)
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> !commandExecutor.hasCommand());

        Mockito.verify(logCommandExceptionHandler, Mockito.times(1)).handle(any(), any());
        Mockito.verify(retryCommandExceptionHandler, Mockito.times(1)).handle(any(), any());
        Mockito.verify(spyCommand, Mockito.times(2)).execute();

        commandExecutor.stop();

    }


    @Test
    void executeRetryRetryLog() {
        CommandPublisher commandPublisher = new CommandPublisherImpl();
        CommandExecuteExceptionHandler commandExecuteExceptionHandler = new CommandExecuteExceptionHandler();
        final CommandExecutor commandExecutor = new CommandExecutor(commandExecuteExceptionHandler);
        commandPublisher.addListener(commandExecutor);

        final LogCommandExceptionHandler logCommandExceptionHandler = Mockito.spy(new LogCommandExceptionHandler(commandPublisher));
        final RetryCommandExceptionHandler retryCommandExceptionHandler = Mockito.spy(new RetryCommandExceptionHandler(commandPublisher, 2));
        commandExecuteExceptionHandler.registerHandler(RetryCommand.class, RetryCommandException.class, logCommandExceptionHandler);
        commandExecuteExceptionHandler.registerHandler(RetryCommand.class, Exception.class, retryCommandExceptionHandler);

        commandExecuteExceptionHandler.registerHandler(TestCommand.class, Exception.class, retryCommandExceptionHandler);

        commandExecutor.start();

        final TestCommand command = Mockito.spy(new TestCommand(new IllegalArgumentException("Ошибка")));
        commandPublisher.publish(command);

        await().pollDelay(1, TimeUnit.SECONDS)
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> !commandExecutor.hasCommand());

        Mockito.verify(logCommandExceptionHandler, Mockito.times(1)).handle(any(), any());
        Mockito.verify(retryCommandExceptionHandler, Mockito.times(2)).handle(any(), any());
        Mockito.verify(command, Mockito.times(3)).execute();

        commandExecutor.stop();
    }

    public static class TestCommand implements Command {

        private final RuntimeException exception;

        public TestCommand(RuntimeException exception) {
            this.exception = exception;
        }

        @Override
        public void execute() {
            throw exception;
        }
    }

}
