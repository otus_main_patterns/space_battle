package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class CheckFuelCommandTest {

    @Test
    void executeWithOkResultTest() {
        final FuelBurnable fuelBurnableMock = Mockito.mock(FuelBurnable.class);
        Mockito.when(fuelBurnableMock.getFuelVelocity()).thenReturn(1);
        Mockito.when(fuelBurnableMock.getFuelLevel()).thenReturn(1);
        final CheckFuelCommand checkFuelCommand = new CheckFuelCommand(fuelBurnableMock);
        checkFuelCommand.execute();
        Mockito.verify(fuelBurnableMock, Mockito.times(1)).getFuelVelocity();
        Mockito.verify(fuelBurnableMock, Mockito.times(1)).getFuelLevel();
    }

    @Test
    void executeWithFailResultTest() {
        final FuelBurnable fuelBurnableMock = Mockito.mock(FuelBurnable.class);
        Mockito.when(fuelBurnableMock.getFuelVelocity()).thenReturn(2);
        Mockito.when(fuelBurnableMock.getFuelLevel()).thenReturn(1);
        final CheckFuelCommand checkFuelCommand = new CheckFuelCommand(fuelBurnableMock);
        Assertions.assertThrowsExactly(CheckFuelCommandException.class, checkFuelCommand::execute);
    }

}