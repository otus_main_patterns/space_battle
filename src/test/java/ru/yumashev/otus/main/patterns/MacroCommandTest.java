package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MacroCommandTest {

    @Test
    void executeOkTest() {

        final Command commandMock1 = Mockito.mock(Command.class);
        final Command commandMock2 = Mockito.mock(Command.class);
        final MacroCommand macroCommand = new MacroCommand(List.of(commandMock1, commandMock2));
        macroCommand.execute();
        Mockito.verify(commandMock1, Mockito.times(1)).execute();
        Mockito.verify(commandMock2, Mockito.times(1)).execute();
    }

    @Test
    void executeFailTest() {
        final Command commandMock1 = Mockito.mock(Command.class);
        final Command commandMock2 = Mockito.mock(Command.class);
        Mockito.doThrow(new RuntimeException()).when(commandMock1).execute();
        final MacroCommand macroCommand = new MacroCommand(List.of(commandMock1, commandMock2));
        Assertions.assertThrows(CommandException.class, macroCommand::execute);
        Mockito.verify(commandMock1, Mockito.times(1)).execute();
        Mockito.verify(commandMock2, Mockito.never()).execute();
    }


    @Test
    void executeMoveViaFuelBurn() {
        final FuelBurnable fuelBurnableMock = Mockito.mock(FuelBurnable.class);
        Mockito.when(fuelBurnableMock.getFuelVelocity()).thenReturn(1);
        Mockito.when(fuelBurnableMock.getFuelLevel()).thenReturn(1);

        final Coords positionBefore = new Coords(12, 5);
        final Coords velocity = new Coords(-7, 3);
        final Coords positionAfter = new Coords(5, 8);
        Movable movableMock = Mockito.mock(Movable.class);
        Mockito.when(movableMock.getVelocity()).thenReturn(velocity);
        Mockito.when(movableMock.getPosition()).thenReturn(positionBefore);

        final Command checkFuelCommandMock = Mockito.spy(new CheckFuelCommand(fuelBurnableMock));
        final Command moveCommandMock = Mockito.spy(new MoveCommand(movableMock));
        final Command burnFuelCommandMock = Mockito.spy(new BurnFuelCommand(fuelBurnableMock));

        final MacroCommand macroCommand = new MacroCommand(List.of(checkFuelCommandMock, moveCommandMock, burnFuelCommandMock));

        macroCommand.execute();

        Mockito.verify(checkFuelCommandMock, Mockito.times(1)).execute();
        Mockito.verify(moveCommandMock, Mockito.times(1)).execute();
        Mockito.verify(burnFuelCommandMock, Mockito.times(1)).execute();

        Mockito.verify(fuelBurnableMock, Mockito.atMostOnce()).setFuelLevel(0);
        Mockito.verify(fuelBurnableMock).setFuelLevel(0);

        Mockito.verify(movableMock, Mockito.atMostOnce()).setPosition(positionAfter);
        Mockito.verify(movableMock).setPosition(positionAfter);
    }

    @Test
    void executeMoveViaFuelBurnFailTest() {
        final FuelBurnable fuelBurnableMock = Mockito.mock(FuelBurnable.class);
        Mockito.when(fuelBurnableMock.getFuelVelocity()).thenReturn(1);
        Mockito.when(fuelBurnableMock.getFuelLevel()).thenReturn(0);

        final Coords positionBefore = new Coords(12, 5);
        final Coords velocity = new Coords(-7, 3);
        final Coords positionAfter = new Coords(5, 8);
        Movable movableMock = Mockito.mock(Movable.class);
        Mockito.when(movableMock.getVelocity()).thenReturn(velocity);
        Mockito.when(movableMock.getPosition()).thenReturn(positionBefore);

        final Command checkFuelCommandMock = Mockito.spy(new CheckFuelCommand(fuelBurnableMock));
        final Command moveCommandMock = Mockito.spy(new MoveCommand(movableMock));
        final Command burnFuelCommandMock = Mockito.spy(new BurnFuelCommand(fuelBurnableMock));

        final MacroCommand macroCommand = new MacroCommand(List.of(checkFuelCommandMock, moveCommandMock, burnFuelCommandMock));

        Assertions.assertThrows(CommandException.class, macroCommand::execute);

        Mockito.verify(checkFuelCommandMock, Mockito.times(1)).execute();
        Mockito.verify(moveCommandMock, Mockito.never()).execute();
        Mockito.verify(burnFuelCommandMock, Mockito.never()).execute();

    }

}