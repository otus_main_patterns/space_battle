package ru.yumashev.otus.main.patterns;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TransactionMacroCommandTest {

    @Test
    void execute() {
        final UndoCommand commandMock1 = Mockito.mock(UndoCommand.class);
        final UndoCommand commandMock2 = Mockito.mock(UndoCommand.class);
        Mockito.doThrow(new RuntimeException()).when(commandMock2).execute();
        final TransactionMacroCommand macroCommand = new TransactionMacroCommand(List.of(commandMock1, commandMock2));
        Assertions.assertThrows(CommandException.class, macroCommand::execute);
        Mockito.verify(commandMock1, Mockito.times(1)).execute();
        Mockito.verify(commandMock2, Mockito.times(1)).execute();
        Mockito.verify(commandMock1, Mockito.times(1)).undo();
    }
}