package ru.yumashev.otus.main.patterns;


public record Coords(int x, int y) {

    public Coords plus(Coords coords) {
        return new Coords(this.x + coords.x, this.y + coords.y);
    }
}
