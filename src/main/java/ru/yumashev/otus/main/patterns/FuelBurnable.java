package ru.yumashev.otus.main.patterns;

public interface FuelBurnable {
    int getFuelLevel();

    void setFuelLevel(int value);

    int getFuelVelocity();
}
