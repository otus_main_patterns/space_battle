package ru.yumashev.otus.main.patterns.ioc;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public abstract class AbsractIOCScope implements IOCScope {
    protected final Map<String, Function<Object[], ?>> containerMap = new ConcurrentHashMap<>();

    @Override
    public <T> T resolve(String key, Class<T> tClass, Object... args) {
        return tClass.cast(Optional.ofNullable(containerMap.get(key)).map(function -> function.apply(args)).orElseThrow(() -> new IOCException("Key not found - " + key)));
    }

    protected <T> T resolve(Function<Object[], T> function, Object... args) {
        return function.apply(args);
    }

    @Override
    public void addResolver(String key, Function<Object[], ?> resolver) {
        containerMap.put(key, resolver);
    }
}
