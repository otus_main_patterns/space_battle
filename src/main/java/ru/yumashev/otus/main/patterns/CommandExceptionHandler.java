package ru.yumashev.otus.main.patterns;

public interface CommandExceptionHandler {
    void handle(Command command, Exception exception);
}
