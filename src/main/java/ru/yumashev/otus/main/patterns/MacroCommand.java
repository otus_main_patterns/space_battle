package ru.yumashev.otus.main.patterns;

import java.util.Collection;

public class MacroCommand implements Command {

    private final Collection<Command> commands;

    public MacroCommand(Collection<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        for (Command command : commands) {
            try {
                command.execute();
            } catch (Exception e) {
                throw new CommandException(e, this);
            }
        }
    }
}
