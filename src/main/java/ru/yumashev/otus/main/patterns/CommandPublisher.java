package ru.yumashev.otus.main.patterns;

public interface CommandPublisher {

    void publish(Command command);

    void addListener(CommandPublisherListener listener);



}
