package ru.yumashev.otus.main.patterns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommandExecuteExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(CommandExecuteExceptionHandler.class);
    private final List<HandlerByCommand> handlerByCommands = new ArrayList<>();

    public void handle(Command command, Exception exception) {
        findHandler(command, exception).ifPresentOrElse(exceptionHandler -> exceptionHandler.handle(command, exception), () -> logger.warn("Exception Handler not found. {}, {}", command, exception));
    }

    private Optional<CommandExceptionHandler> findHandler(Command command, Exception exception) {
        return findHandlerByCommand(command).flatMap(handlerByCommand -> handlerByCommand.handlerByExceptions().stream().filter(e -> e.exceptionClass().isAssignableFrom(exception.getClass())).findFirst().map(HandlerByException::exceptionHandler));
    }

    private Optional<HandlerByCommand> findHandlerByCommand(Command command) {
        return handlerByCommands.stream().filter(handlerByCommand -> handlerByCommand.commandClass.isInstance(command)).findFirst();
    }

    public void registerHandler(Class<? extends Command> commandClass, Class<? extends Exception> exceptionClass, CommandExceptionHandler exceptionHandler) {
        final Optional<HandlerByCommand> optionalHandlerByCommand = handlerByCommands.stream().filter(handlerByCommand -> handlerByCommand.commandClass().isAssignableFrom(commandClass)).findFirst();
        if (optionalHandlerByCommand.isPresent()) {
            optionalHandlerByCommand.get().handlerByExceptions().add(new HandlerByException(exceptionClass, exceptionHandler));
        } else {
            final ArrayList<HandlerByException> handlerByExceptions = new ArrayList<>();
            handlerByExceptions.add(new HandlerByException(exceptionClass, exceptionHandler));
            handlerByCommands.add(new HandlerByCommand(commandClass, handlerByExceptions));
        }
    }


    record HandlerByCommand(Class<? extends Command> commandClass, List<HandlerByException> handlerByExceptions) {
    }

    record HandlerByException(Class<? extends Exception> exceptionClass, CommandExceptionHandler exceptionHandler) {
    }

}
