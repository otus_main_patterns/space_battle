package ru.yumashev.otus.main.patterns;

public class BurnFuelCommand implements Command {

    private final FuelBurnable fuelBurnable;

    public BurnFuelCommand(FuelBurnable fuelBurnable) {
        this.fuelBurnable = fuelBurnable;
    }

    @Override
    public void execute() {
        final int newFuelLevel = fuelBurnable.getFuelLevel() - fuelBurnable.getFuelVelocity();
        if (newFuelLevel < 0) {
            throw new CheckFuelCommandException(this);
        } else {
            fuelBurnable.setFuelLevel(newFuelLevel);
        }
    }

}
