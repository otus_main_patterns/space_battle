package ru.yumashev.otus.main.patterns;

public interface CommandPublisherListener {
    void onCommand(Command command);
}
