package ru.yumashev.otus.main.patterns;

public class CheckFuelCommand implements Command {

    private final FuelBurnable fuelBurnable;

    public CheckFuelCommand(FuelBurnable fuelBurnable) {
        this.fuelBurnable = fuelBurnable;
    }

    @Override
    public void execute() {
        if (fuelBurnable.getFuelLevel() - fuelBurnable.getFuelVelocity() < 0) {
            throw new CheckFuelCommandException(this);
        }
    }

}
