package ru.yumashev.otus.main.patterns.ioc;

public class IOCException extends RuntimeException {
    public IOCException(String message) {
        super(message);
    }
}
