package ru.yumashev.otus.main.patterns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RetryCommand implements Command {


    private final Logger logger = LoggerFactory.getLogger(RetryCommand.class);

    private final Command command;
    private final int maxRetries;

    private int retryCount = 0;


    public RetryCommand(Command command, int maxRetries) {
        if (command instanceof RetryCommand retryCommand) {
            this.command = retryCommand.command;
            this.retryCount = retryCommand.retryCount;
        } else {
            this.command = command;
        }
        this.maxRetries = maxRetries;
    }

    @Override
    public void execute() {
        try {
            retryCount++;
            logger.info("Retry {} command {}", retryCount, command);
            command.execute();
        } catch (Exception exception) {
            if (retryCount == maxRetries) {
                throw new RetryCommandException(command, exception);
            } else {
                throw exception;
            }
        }
    }

    @Override
    public String toString() {
        return "RetryCommand{" +
                "command=" + command +
                '}';
    }
}
