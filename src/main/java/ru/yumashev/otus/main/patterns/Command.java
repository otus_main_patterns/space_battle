package ru.yumashev.otus.main.patterns;

public interface Command {
    void execute();
}
