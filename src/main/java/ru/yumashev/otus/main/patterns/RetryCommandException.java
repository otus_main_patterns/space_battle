package ru.yumashev.otus.main.patterns;

public class RetryCommandException extends RuntimeException {

    private final Command command;

    public RetryCommandException(Command command, Exception cause) {
        super(cause);
        this.command = command;
    }

}
