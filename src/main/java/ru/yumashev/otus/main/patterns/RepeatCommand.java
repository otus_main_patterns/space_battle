package ru.yumashev.otus.main.patterns;

public class RepeatCommand implements Command {

    private final Command command;
    private final CommandPublisher commandPublisher;
    private int times;

    public RepeatCommand(Command command, CommandPublisher commandPublisher, int times) {
        this.command = command;
        this.commandPublisher = commandPublisher;
        this.times = times;
    }

    @Override
    public void execute() {
        if (times > 0) {
            commandPublisher.publish(command);
            times--;
            commandPublisher.publish(this);
        }
    }
}
