package ru.yumashev.otus.main.patterns.ioc;

public class IOCScopeItem extends AbsractIOCScope {

    private final IOCScope parent;

    public IOCScopeItem(IOCScope parent) {
        this.parent = parent;
    }

    public static IOCScope of(IOCScope iocScope) {
        return new IOCScopeItem(iocScope);
    }

    @Override
    public <T> T resolve(String key, Class<T> tClass, Object... args) {
        if (containerMap.containsKey(key)) {
            return tClass.cast(resolve(containerMap.get(key), args));
        } else {
            return parent.resolve(key, tClass, args);
        }
    }


}
