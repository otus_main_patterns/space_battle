package ru.yumashev.otus.main.patterns.ioc;

import java.util.Map;
import java.util.function.Function;

public class RootIOCScope extends AbsractIOCScope {

    public RootIOCScope(Map<String, Function<Object[], ?>> containerMap) {
        this.containerMap.putAll(containerMap);
    }

    public static RootIOCScope of(Map<String, Function<Object[], ?>> containerMap) {
        return new RootIOCScope(containerMap);
    }

}
