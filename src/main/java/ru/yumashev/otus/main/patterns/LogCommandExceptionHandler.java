package ru.yumashev.otus.main.patterns;

public class LogCommandExceptionHandler implements CommandExceptionHandler {

    private final CommandPublisher commandPublisher;

    public LogCommandExceptionHandler(CommandPublisher commandPublisher) {
        this.commandPublisher = commandPublisher;
    }

    @Override
    public void handle(Command command, Exception exception) {
        commandPublisher.publish(new LogExceptionCommand(command, exception));
    }
}
