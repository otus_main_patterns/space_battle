package ru.yumashev.otus.main.patterns;

public interface Rotatable {
    int getAngle();

    void setAngle(int angle);

    int getAngularVelocity();
}
