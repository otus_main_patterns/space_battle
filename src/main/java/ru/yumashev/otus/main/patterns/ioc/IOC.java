package ru.yumashev.otus.main.patterns.ioc;

import java.util.function.Function;

public interface IOC {

    <T> T resolve(String key, Class<T> tClass, Object... args);

    void addResolver(String key, Function<Object[], ?> function);

}
