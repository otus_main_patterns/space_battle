package ru.yumashev.otus.main.patterns;

public class RotateCommand implements Command {

    private final Rotatable rotatable;

    public RotateCommand(Rotatable rotatable) {
        this.rotatable = rotatable;
    }

    @Override
    public void execute() {
        if (rotatable.getAngularVelocity() >= 180) {
            throw new TooFastRotateException();
        }
        this.rotatable.setAngle(this.rotatable.getAngle() + (this.rotatable.getAngularVelocity()));
    }

}
