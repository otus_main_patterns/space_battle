package ru.yumashev.otus.main.patterns.ioc;

import ru.yumashev.otus.main.patterns.Command;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class IOCImp implements IOC {
    private final RootIOCScope rootIOCScope;

    private final ThreadLocal<IOCScope> currentScope = new ThreadLocal<>();
    private final Map<String, IOCScope> scopeMap = new ConcurrentHashMap<>();

    public IOCImp() {
        Map<String, Function<Object[], ?>> containerMap = new HashMap<>();
        containerMap.put("IoC.Register", args -> new IOCRegisterCommand(this, args));
        containerMap.put("IoC.Scope.Root", args -> new Command() {
            @Override
            public void execute() {
                currentScope.set(rootIOCScope);
            }
        });
        containerMap.put("IoC.Scope.Create", args ->
                (Command) () -> {
                    final String scopeId = (String) args[0];
                    if (args.length == 2) {
                        final String parentScopeId = (String) args[1];
                        final IOCScope iocScope = scopeMap.get(parentScopeId);
                        scopeMap.put(scopeId, IOCScopeItem.of(iocScope));
                    } else {
                        scopeMap.put(scopeId, IOCScopeItem.of(getCurrentScope()));
                    }
                });
        containerMap.put("IoC.Scope.Set", args ->
                (Command) () -> {
                    final String scopeId = (String) args[0];
                    currentScope.set(Objects.requireNonNull(scopeMap.get(scopeId)));
                });

        this.rootIOCScope = RootIOCScope.of(containerMap);
        currentScope.set(rootIOCScope);
        scopeMap.put("root", rootIOCScope);
    }

    @Override
    public <T> T resolve(String key, Class<T> tClass, Object... args) {
        return getCurrentScope().resolve(key, tClass, args);
    }

    @Override
    public void addResolver(String key, Function<Object[], ?> function) {
        getCurrentScope().addResolver(key, function);
    }

    private IOCScope getCurrentScope() {
        if (currentScope.get() == null) {
            currentScope.set(rootIOCScope);
        }
        return currentScope.get();
    }
}
