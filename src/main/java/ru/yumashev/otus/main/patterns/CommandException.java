package ru.yumashev.otus.main.patterns;

public class CommandException extends RuntimeException {

    private final transient Command command;

    public CommandException(Command command) {
        this("Error while executing command " + command, command);
    }

    public CommandException(String message, Command command) {
        super(message);
        this.command = command;
    }

    public CommandException(String message, Throwable cause, Command command) {
        super(message, cause);
        this.command = command;
    }

    public CommandException(Throwable cause, Command command) {
        super(cause);
        this.command = command;
    }

    public CommandException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Command command) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}
