package ru.yumashev.otus.main.patterns;

public interface Movable {
    Coords getPosition();

    void setPosition(Coords position);

    Coords getVelocity();
}
