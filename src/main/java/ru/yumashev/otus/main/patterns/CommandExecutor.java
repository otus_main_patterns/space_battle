package ru.yumashev.otus.main.patterns;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class CommandExecutor implements CommandPublisherListener {

    private final Queue<Command> commands = new ArrayBlockingQueue<>(100);

    private final CommandExecuteExceptionHandler commandExecuteExceptionHandler;

    private boolean stop = true;

    public CommandExecutor(CommandExecuteExceptionHandler commandExecuteExceptionHandler) {
        this.commandExecuteExceptionHandler = commandExecuteExceptionHandler;
    }

    public void start() {
        stop = false;
        new Thread(() -> {
            while (!stop) {
                final Command command = commands.poll();
                if (command != null) {
                    try {
                        command.execute();
                    } catch (Exception exception) {
                        commandExecuteExceptionHandler.handle(command, exception);
                    }
                }
            }
        }).start();
    }

    public void stop() {
        stop = true;
    }

    public void addCommand(Command command) {
        commands.offer(command);
    }

    public boolean isStop() {
        return stop;
    }

    @Override
    public void onCommand(Command command) {
        addCommand(command);
    }

    public Boolean hasCommand() {
        return !commands.isEmpty();
    }
}
