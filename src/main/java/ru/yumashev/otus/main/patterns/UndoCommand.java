package ru.yumashev.otus.main.patterns;

public interface UndoCommand extends Command {
    void undo();
}
