package ru.yumashev.otus.main.patterns;

import java.util.*;

public class TransactionMacroCommand implements Command {

    private final Collection<UndoCommand> commands;

    public TransactionMacroCommand(Collection<UndoCommand> commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        Deque<UndoCommand> undoCommands = new ArrayDeque<>();
        for (UndoCommand command : commands) {
            try {
                command.execute();
                undoCommands.push(command);
            } catch (Exception e) {
                while (!undoCommands.isEmpty()) {
                    undoCommands.pop().undo();
                }
                throw new CommandException(e, this);
            }
        }
    }
}
