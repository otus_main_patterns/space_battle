package ru.yumashev.otus.main.patterns;

public class CheckFuelCommandException extends CommandException {

    public CheckFuelCommandException(Command command) {
        super("Fuel is not enough", command);
    }

}
