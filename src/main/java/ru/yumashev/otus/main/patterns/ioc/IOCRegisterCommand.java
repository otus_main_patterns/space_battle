package ru.yumashev.otus.main.patterns.ioc;

import ru.yumashev.otus.main.patterns.Command;

import java.util.Map;
import java.util.function.Function;

public class IOCRegisterCommand implements Command {
    private final Object[] args;
    private final IOC ioc;

    public IOCRegisterCommand(IOC ioc, Object[] args) {
        this.args = args;
        this.ioc = ioc;
    }

    @Override
    public void execute() {
        if (args.length == 0) {
            throw new IOCException("Illegal Arguments. Must by key in args");
        }
        final String key = (String) args[0];
        final Function<Object[], ?> createFunction = (Function<Object[], ?>) args[1];
        ioc.addResolver(key, createFunction);
    }
}
