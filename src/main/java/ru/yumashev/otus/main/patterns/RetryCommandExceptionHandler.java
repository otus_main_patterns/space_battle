package ru.yumashev.otus.main.patterns;

public class RetryCommandExceptionHandler implements CommandExceptionHandler {

    private final CommandPublisher commandPublisher;
    private final int maxRetries;

    public RetryCommandExceptionHandler(CommandPublisher commandPublisher, int maxRetries) {
        this.commandPublisher = commandPublisher;
        this.maxRetries = maxRetries;
    }

    public RetryCommandExceptionHandler(CommandPublisher commandPublisher) {
        this(commandPublisher, 1);
    }

    @Override
    public void handle(Command command, Exception exception) {
        commandPublisher.publish(new RetryCommand(command, maxRetries));
    }
}
