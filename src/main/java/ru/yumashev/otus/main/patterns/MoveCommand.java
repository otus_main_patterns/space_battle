package ru.yumashev.otus.main.patterns;

public class MoveCommand implements Command {

    private final Movable movable;

    public MoveCommand(Movable movable) {
        this.movable = movable;
    }

    @Override
    public void execute() {
        this.movable.setPosition(this.movable.getPosition().plus(this.movable.getVelocity()));
    }

}
