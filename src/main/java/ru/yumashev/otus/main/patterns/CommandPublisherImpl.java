package ru.yumashev.otus.main.patterns;

import java.util.ArrayList;
import java.util.List;

public class CommandPublisherImpl implements CommandPublisher {

    private final List<CommandPublisherListener> listeners = new ArrayList<>();

    @Override
    public void publish(Command command) {
        listeners.forEach(commandPublisherListener -> commandPublisherListener.onCommand(command));
    }

    @Override
    public void addListener(CommandPublisherListener listener) {
        listeners.add(listener);
    }

}
